from flask import Flask, render_template, flash, redirect, request, send_file, send_from_directory
from stegano import lsb
from cryptography.fernet import Fernet
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from forms import MessageInputForm, PhotoForm, CryptoMessageForm
from config import Config
import re, os
import base64
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = 'static/uploads'
STEGANO_FOLDER = 'static/steganographed'
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)
app.config.from_object(Config)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['STEGANO_FOLDER'] = STEGANO_FOLDER


@app.route('/')
def base():
    return redirect('/index')

@app.route('/index')
def index():
    return render_template('index.html', message=True)

# This path is a secret path, one can only find it if one is unwilling to search
@app.route('/hide', methods=["GET", "POST"])
def hide():
    selectedFile = request.form.get("file")
    form = MessageInputForm(request.form)
    if selectedFile:
        flash(f"Selected file is : {selectedFile.split('/')[-1]}")
        form = MessageInputForm(request.form, image = selectedFile)
    if form.validate():
        imagepath=form.image.data.split("/")[-1]
        message=form.input.data
        secret = lsb.hide(form.image.data, message)
        secretpath = imagepath.split(".")[0]
        secretpath = f"{secretpath}-secret.png"
        secret.save(os.path.join(app.config['STEGANO_FOLDER'],secretpath))
        return send_from_directory(STEGANO_FOLDER, secretpath, as_attachment=True)

    return render_template('hide.html', form=form)

@app.route('/crypto', methods=["GET", "POST"])
def crypto():
    form = CryptoMessageForm(request.form)
    if form.validate():
        password_provided = form.key.data
        password = password_provided.encode()
        salt = b'BV\xc4\xd2f7\xc9G\xca\xd9\xa7i\xfe\xa1\xcf\xe0'
        kdf = PBKDF2HMAC(
            algorithm=hashes.SHA256(),
            length=32,
            salt=salt,
            iterations=100000,
            backend=default_backend()
        )
        key = base64.urlsafe_b64encode(kdf.derive(password))
        keyf = Fernet(key)
        try:
            if (form.encrypt.data):
                encryption = keyf.encrypt(form.input.data.encode())
                flash(f"Encrypt: {encryption.decode()}")
                return render_template('crypto.html', form=form)
            else:
                decryption = keyf.decrypt(form.input.data.encode())
                flash(f"Decrypt: {decryption.decode()}")
                return render_template('crypto.html', form=form)
        except:
            flash("Something went wrong, it's likely that the data you tried to\
             decrypt were not actually encrypted. Or the key was wrong \
             (Try checking the encrypt box). \
             If the error persists, I have no clue what it might be.")
            return render_template('crypto.html', form=form)
    return render_template('crypto.html', form=form)


@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    form = PhotoForm(csrf_enabled=False)
    if form.validate_on_submit():
        f = form.photo.data
        filename = secure_filename(f.filename)
        if not os.path.exists(app.config['UPLOAD_FOLDER']):
            os.makedirs(app.config['UPLOAD_FOLDER'])
        f.save(os.path.join(app.config['UPLOAD_FOLDER'],filename))
        flash(f"{filename} was corectly saved on the server!")
        return redirect('/files')

    return render_template('upload.html', form=form)

@app.route('/files')
def files():
    filesDir = os.listdir('static/uploads')
    fileList = ['uploads/' + file for file in filesDir]
    return render_template('files.html', fileList = fileList)

@app.route("/delete",methods =["GET","POST"])
def delete():
    if request.method =="POST":
        file = request.form.get("file")
        os.remove(file)
        flash(f"{file.split('/')[-1]} was sucessfully deleted!")
    return redirect("/files")

@app.route("/reveal",methods =["GET","POST"])
def reveal():
    if request.method == "POST":
        file = request.form.get("file")
        flash(lsb.reveal(file))
    return redirect("/files")
