from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, HiddenField, TextAreaField, BooleanField
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms.validators import DataRequired
from werkzeug.utils import secure_filename



class MessageInputForm(FlaskForm):
    input = TextAreaField('Message', validators=[DataRequired()])
    image = HiddenField('Image')
    submit = SubmitField('Submit')


class PhotoForm(FlaskForm):
    photo = FileField(validators=[
        FileRequired(),
        FileAllowed(['jpg', 'png', 'gif'], 'Images only!')
    ])
    submit = SubmitField('Submit')

class CryptoMessageForm(FlaskForm):
    input = StringField('Message', validators=[DataRequired()])
    key = StringField('Key', validators=[DataRequired()])
    encrypt = BooleanField('Encrypt')
    submit = SubmitField('Submit')
