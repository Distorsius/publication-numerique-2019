**In a nutshell**

Small scale project in creating a Flask web app that allow basic cryptographic and steganographic functions trough the web.
Yay.
Requires an installation of python 3.4+

On linux and unix-like systems, to run localy, open a terminal in the installation folder, and type: 

source flaskvenv/bin/activate

export FLASK_APP = app.py

flask run

The app will then run on localhost:5000

*If you are on windows, guides explain the long and convoluted process to launch a flask application.*

**Basic functionalities**

The web app in comprised of 2 main functionalities: a basic cryptographic function and a basic steganographic function.

The first is contained in the "Crypto" tab. 
There you can input a message to be crypted, and a key to encrypt it. The output will then appear at the top of the page to be copied and pasted elsewhere.
You can also input a crypted message, and the same key used to encrypt it, and the original message will appear at the top of the page again.

The steganographic functionality is found in the upload file and all files tab.
Upload file allows you to upload an image file to the server so it can use it. (Prefered formats are .png and .jpeg, the site only outputs in .png).
All file is a list of all available files for later use. It can be used both to steganographically hide text strings inside images or reveal previously hidden strings from images.
Press the Hide in button next to any image to obtain the hiding interface, wich allows you to input a message, and allows you to download an image file with the hidden message.
Press the Reveal button to output at the top of the page any text found in the image.

